#include <iostream>
#include <Compte.h>
using namespace std;
#include <string>

int main()
{
    int choix;
    Compte c;
    c.setSolde(50000);
    c.setMdp(1234);

    c.Connexion();

    if (c.getConnected() == true){
        choix = c.choice();
        switch(choix){
        case 1:
            c.retrait(c.sommeRetrait());
        case 2:
            c.consultation();
        case 3:
            c.quitter();
            c.~Compte();
        }
    }
    return 0;
}
