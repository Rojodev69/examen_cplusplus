#ifndef COMPTE_H
#define COMPTE_H
#include <string>
using namespace std;

class Compte
{
    public:
        Compte();
        virtual ~Compte();
        bool Connexion();
        int sommeRetrait();
        int choice();
        void consultation();
        void quitter();
        void retrait(int som);

        //solde
        int getSolde();
        void setSolde(int s);
        //mot de passe
        int getMdp();
        void setMdp(int m);
        //cetat connection
        bool getConnected();
        void setConnected(bool conn);
    protected:

    private:
        int solde;
        bool valide;
        int mdp;
        bool connected;
};

#endif // COMPTE_H
